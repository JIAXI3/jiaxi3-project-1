import bagel.DrawOptions;
import bagel.Font;
import bagel.Image;
import bagel.util.Point;
import bagel.util.Rectangle;

/**
 * Player Code for SWEN20003 Project 1, Semester 2, 2022
 * <p>
 * Manage world info.
 * Manage player life.
 * Player move.
 * Draw player.
 *
 * @JIAXI3
 */
public class Player {
    private final static Image FaceLeftImage = new Image("res/faeLeft.png");
    private final static Image FaceRightImage = new Image("res/faeRight.png");

    private static final int LIFE_MAX = 100;
    private static int Life = LIFE_MAX;
    private static Image Img;
    private final Font LifeFont = new Font("res/frostbite.ttf", 30);
    private final DrawOptions LifeHighColor = new DrawOptions();
    private final DrawOptions LifeMidColor = new DrawOptions();
    private final DrawOptions LifeLowColor = new DrawOptions();
    private int X = 0;
    private int Y = 0;

    private int WORLD_TOP = 0;
    private int WORLD_LEFT = 0;
    private int WORLD_BOTTOM = 0;
    private int WORLD_RIGHT = 0;

    public Player() {
        /*Init player image*/
        Img = FaceRightImage;

        /*Init life color*/
        LifeHighColor.setBlendColour(0, 0.8, 0.2);
        LifeMidColor.setBlendColour(0.9, 0.6, 0);
        LifeLowColor.setBlendColour(1, 0, 0);
    }

    /**
     * Reset all properties.
     */
    public void Clear() {
        X = 0;
        Y = 0;
        WORLD_TOP = 0;
        WORLD_LEFT = 0;
        WORLD_BOTTOM = 0;
        WORLD_RIGHT = 0;
        Life = LIFE_MAX;
    }

    /**
     * Get player position.
     */
    public Point GetPos() {
        /*Get the position of player*/
        return new Point(X, Y);
    }

    /**
     * Set World top left position.
     */
    public void SetWorldTopLeft(int top, int left) {
        /*Set the position of world TopLeft*/
        WORLD_TOP = top;
        WORLD_LEFT = left;
    }

    /**
     * Set World bottom right position.
     */
    public void SetWorldBottomRight(int bottom, int right) {
        /*Set the position of world BottomRight*/
        WORLD_BOTTOM = bottom;
        WORLD_RIGHT = right;
    }

    /**
     * Set player position.
     */
    public void SetPos(Point p) {
        /*Set the position of player*/
        X = (int) p.x;
        Y = (int) p.y;
    }

    /**
     * Check whether the player falls into a hole.
     */
    public int IntoHole() {
        /*Life handling and system output for players falling into sinkholes*/
        Life -= 30;
        if (Life < 0) {
            Life = 0;
        }
        System.out.println("Sinkhole inflicts 30 damage points on Fae.  Fae's current health:  " + Life + "/" + LIFE_MAX);
        return Life;
    }

    /**
     * Get player Rectangle.
     */
    public Rectangle GetRectangle() {
        /*Get Rectangle of player*/
        return Img.getBoundingBoxAt(GetPos());
    }

    /**
     * Player takes a step up.
     */
    public void MoveUp() {
        /*Player moves up in world, step = 2*/
        if (Y > (WORLD_TOP + 2)) {
            Y -= 2;
        }
    }

    /**
     * Player takes a step down.
     */
    public void MoveDown() {
        /*Player moves down in world, step = 2*/
        if (Y < (WORLD_BOTTOM - 2)) {
            Y += 2;
        }
    }

    /**
     * Player takes a step left.
     */
    public void MoveLeft() {
        /*Player moves left in world, step = 2*/
        if (X > (WORLD_LEFT + 2)) {
            X -= 2;
            Img = FaceLeftImage;
        }
    }

    /**
     * Player takes a step right.
     */
    public void MoveRight() {
        /*Player moves right in world, step = 2*/
        if (X < (WORLD_RIGHT - 2)) {
            X += 2;
            Img = FaceRightImage;
        }
    }

    /**
     * Draw play and life percent.
     */
    public void Update() {
        /*Show play image*/
        Img.drawFromTopLeft(X, Y);

        int life_percent = Life * 100 / LIFE_MAX;
        /*Show life text*/
        if (life_percent >= 65) {
            LifeFont.drawString(life_percent + "%", 20, 25, LifeHighColor);
        } else if (life_percent >= 35) {
            LifeFont.drawString(life_percent + "%", 20, 25, LifeMidColor);
        } else {
            LifeFont.drawString(life_percent + "%", 20, 25, LifeLowColor);
        }
    }

}
