import bagel.Image;
import bagel.util.Rectangle;

import java.util.ArrayList;

/**
 * object Code for SWEN20003 Project 1, Semester 2, 2022
 * <p>
 * Manage object info.
 * Draw object.
 * Determine the overlap between player and object.
 *
 * @JIAXI3
 */

public class GameObj {
    private Image Img;
    private final ArrayList Pos = new ArrayList();
    private final ArrayList rect = new ArrayList();

    GameObj(String img_path) {
        Img = new Image(img_path);
    }

    /**
     * Clear all objects.
     */
    public void Clear() {
        /*Clear all position and Rectangle list*/
        Pos.clear();
        rect.clear();
    }

    /**
     * Draw all objects.
     */
    public void Update() {
        /*Gets all the object locations of the ArrayList for drawing*/
        rect.clear();
        for (int i = 0; i < Pos.size(); i++) {
            bagel.util.Point p = (bagel.util.Point) Pos.get(i);
            Img.drawFromTopLeft(p.x, p.y);

            /*Record all object Rectangle for overlaps checking.*/
            rect.add(Img.getBoundingBoxAt(p));
        }
    }

    /**
     * Intersect check on all objects.
     */
    public boolean IntersectCheck(Rectangle r, boolean remove) {
        /*Check that each wall overlaps the r(player) position*/
        for (int i = 0; i < Pos.size(); i++) {
            if (r.intersects((Rectangle) rect.get(i))) {
                /*Remove the object if the overlap is found, which means the target can only be hit once.*/
                if (remove) {
                    Pos.remove(i);
                }
                return true;
            }
        }
        return false;
    }

    /**
     * Intersect check on all objects.
     */
    public boolean IntersectCheck(Rectangle r) {
        return IntersectCheck(r, false);
    }

    /**
     * Add a object.
     */
    public void Add(int x, int y) {
        /*Add the object position to arraylist*/
        Pos.add(new bagel.util.Point(x, y));
    }
}

