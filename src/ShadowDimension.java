import bagel.*;
import bagel.util.Colour;
import bagel.util.Point;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Skeleton Code for SWEN20003 Project 1, Semester 2, 2022
 * <p>
 * Init and show Welcome.
 * Load CSV file.
 * Get keyboard input and act accordingly.
 * Game state manage.
 *
 * @JIAXI3
 */


public class ShadowDimension extends AbstractGame {
    private final static int WINDOW_WIDTH = 1024;
    private final static int WINDOW_HEIGHT = 768;
    private final static String GAME_TITLE = "SHADOW DIMENSION";
    private static int game_state = 0;
    private final Image BACKGROUND_IMAGE = new Image("res/background0.png");
    private final Font title = new Font("res/frostbite.ttf", 75);
    private final Font hint = new Font("res/frostbite.ttf", 40);
    Player player = new Player();
    GameObj wall = new GameObj("res/wall.png");
    GameObj hole = new GameObj("res/sinkhole.png");


    public ShadowDimension() {
        super(WINDOW_WIDTH, WINDOW_HEIGHT, GAME_TITLE);
    }

    /**
     * The entry point for the program.
     */
    public static void main(String[] args) {
        ShadowDimension game = new ShadowDimension();
        game.run();
    }

    /**
     * Method used to read file and create objects (You can change this
     * method as you wish).
     */
    private void readCSV() {
        String str = null;
        String[] csv;
        try {
            /*Read CSV file*/
            str = new String(Files.readAllBytes(Paths.get("res/level0.csv")));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        /*Split csv text by \r\n or \n, get all line.*/
        csv = str.split("\r\n|\n");

        /*For all line*/
        for (int i = 0; i < csv.length; i++) {
            /*Split by ',' , get data.*/
            String[] line = csv[i].split(",");
            if (line[0].equals("Player")) {
                /*Set player pos*/
                player.SetPos(new Point(Integer.parseInt(line[1]), Integer.parseInt(line[2])));
            } else if (line[0].equals("Wall")) {
                /*Set wall pos*/
                wall.Add(Integer.parseInt(line[1]), Integer.parseInt(line[2]));
            } else if (line[0].equals("Sinkhole")) {
                /*Set sinkhole pos*/
                hole.Add(Integer.parseInt(line[1]), Integer.parseInt(line[2]));
            } else if (line[0].equals("TopLeft")) {
                /*Set world top left pos*/
                player.SetWorldTopLeft(Integer.parseInt(line[2]), Integer.parseInt(line[1]));
            } else if (line[0].equals("BottomRight")) {
                /*Set world top bottom right pos*/
                player.SetWorldBottomRight(Integer.parseInt(line[2]), Integer.parseInt(line[1]));
            }
        }
    }

    /**
     * GameOver function.
     */
    private void GameOver(boolean win) {
        /*Modify the game state at the end of the game*/
        if (win) {
            game_state = 255;
        } else {
            game_state = 254;
        }

    }

    /**
     * Player in hole check function.
     */
    private void InHoleCheck() {
        /*Check if the player falls into a sinkhole.
        If the player runs out of health, the game fails.*/
        if (hole.IntersectCheck(player.GetRectangle(), true)) {
            if (player.IntoHole() <= 0) {
                GameOver(false);
            }
        }
    }

    /**
     * Draw the world, walls, sinkholes, players.
     */
    private void ImgRefresh() {
        BACKGROUND_IMAGE.draw(Window.getWidth() / 2.0, Window.getHeight() / 2.0);
        wall.Update();
        hole.Update();
        player.Update();
    }

    private void Init() {
        /*Init for new turn.*/
        player.Clear();
        wall.Clear();
        hole.Clear();
    }

    /**
     * Process the corresponding keyboard input and information according
     * to the game state and update the display screen.
     */
    @Override
    protected void update(Input input) {
        /*State for game
         * 0:Init and show welcom page
         * 1:First show play page
         * 2:Play
         * 254:Show failure page
         * 255:Show win page
         * */
        switch (game_state) {
            case 0: {
                /*Draw Text*/
                Init();
                Drawing.drawRectangle(0, 0, 1024, 768,
                        new Colour(103.0 / 256, 153.0 / 256, 231.0 / 256));
                title.drawString("SHADOW DIMENSION", 260, 250);
                hint.drawString("PRESS SPACE TO START\nUSE ARROW KEYS TO FIND GATE",
                        (Window.getWidth() - hint.getWidth("USE ARROW KEYS TO FIND GATE") + 90) / 2,
                        (Window.getHeight() - 80 + 190) / 2);

                /*Wait for space key*/
                if (input.wasPressed(Keys.SPACE)) {
                    /*Change game state to 1(play page)*/
                    game_state = 1;
                    /*Read csv file and get info of player & wall & hole & world size*/
                    readCSV();
                }
                break;
            }

            case 1: {
                ImgRefresh();
                game_state = 2;
                break;
            }
            case 2: {
                /*After the player hits the wall,
                he should return to the original position and copy the player's current position.*/
                Point old_p = player.GetPos();

                /*Get key input and move player*/
                if (input.isDown(Keys.UP)) {
                    player.MoveUp();
                } else if (input.isDown(Keys.DOWN)) {
                    player.MoveDown();
                } else if (input.isDown(Keys.RIGHT)) {
                    player.MoveRight();
                } else if (input.isDown(Keys.LEFT)) {
                    player.MoveLeft();
                }

                /*Restore the original position after hitting the wall.*/
                if (wall.IntersectCheck(player.GetRectangle())) {
                    player.SetPos(old_p);
                }

                /*Win check*/
                if ((player.GetPos().x >= 950) && (player.GetPos().y >= 670)) {
                    GameOver(true);
                }

                /*Check if the player has fallen into a sinkhole*/
                InHoleCheck();
				
				/*Show image*/
                ImgRefresh();
                break;
            }

            case 254: {
                /*Draw the failure of the game.*/
                Drawing.drawRectangle(0, 0, 1024, 768,
                        new Colour(103.0 / 256, 153.0 / 256, 231.0 / 256));
                title.drawString("GAME OVER!", (Window.getWidth() - title.getWidth("GAME OVER!")) / 2,
                        Window.getHeight() / 2);

                /*Press the space key to back welcome page.*/
                if (input.wasPressed(Keys.SPACE)) {
                    game_state = 0;
                    readCSV();
                }
                break;
            }

            case 255: {
                /*Draw a winning picture of the game.*/
                Drawing.drawRectangle(0, 0, 1024, 768,
                        new Colour(103.0 / 256, 153.0 / 256, 231.0 / 256));
                title.drawString("CONGRATULATIONS!",
                        (Window.getWidth() - title.getWidth("CONGRATULATIONS!")) / 2,
                        Window.getHeight() / 2);

                /*Press the space key to back welcome page.*/
                if (input.wasPressed(Keys.SPACE)) {
                    game_state = 0;
                    readCSV();
                }
                break;
            }
            default: {

                break;
            }
        }

        /*Game exit*/
        if (input.wasPressed(Keys.ESCAPE)) {
            Window.close();
        }

    }
}
